
    -- Base
import XMonad
import System.Directory ()
import System.IO (hPutStrLn)
import System.Exit (exitSuccess)
import qualified XMonad.StackSet as W

    -- Actions
import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.CycleWS (Direction1D(..), moveTo, shiftTo, WSType(..), nextScreen, prevScreen)
import XMonad.Actions.GridSelect ()
import XMonad.Actions.MouseResize ( mouseResize )
import XMonad.Actions.Promote ()
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Actions.WithAll (sinkAll, killAll)
import qualified XMonad.Actions.Search as S

    -- Data
import Data.Char (isSpace, toUpper)
import Data.Monoid ( Endo )
import Data.Maybe ( fromJust, isJust )
import Data.Tree ()
import qualified Data.Map as M

    -- Hooks
import XMonad.Hooks.DynamicLog (PP (..), dynamicLogWithPP, shorten, wrap, xmobarColor, xmobarPP)
import XMonad.Hooks.EwmhDesktops ( ewmh )  -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.ManageDocks -- (avoidStruts, docksEventHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, doCenterFloat)
import XMonad.Hooks.ServerMode
import XMonad.Hooks.SetWMName
import XMonad.Hooks.WorkspaceHistory

    -- Layouts
import XMonad.Layout.Accordion
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

    -- Layouts modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

   -- Utilities
import XMonad.Util.Dmenu
import XMonad.Util.EZConfig -- (additionalKeys)
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run -- (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce


myFont :: String
myFont = "xft:JetBrains Mono:style=Regular:size=10:antialias=true:hinting=true"

myModMask :: KeyMask
myModMask = mod4Mask -- Sets modkey to super/windows key

myTerminal :: String
myTerminal = "alacritty" -- Sets default terminal

myBrowser :: String
myBrowser = "firefox " -- Sets firefox as browser

myFileManager :: String
myFileManager = "nemo" -- Sets pcmanfm as file manager

myBorderWidth :: Dimension
myBorderWidth = 2 -- Sets border width for windows

myNormalBorderColor :: String
myNormalBorderColor = "#434c5e"

myFocusedBorderColor :: String
myFocusedBorderColor = "#2e3440"

myStartupHook :: X ()
myStartupHook = do
    spawnOnce "nitrogen --restore &"
    -- spawnOnce "picom &"
    
    -- Workspace layout on the monitors
    screenWorkspace 2 >>= flip whenJust (windows . W.view) -- Focus the second screen.
    windows $ W.greedyView "TV" -- Force the second screen to "tv"

    screenWorkspace 1 >>= flip whenJust (windows . W.view) -- Focus the first screen.
    windows $ W.greedyView "DOCS" -- Force the first screen to "main"
    
    screenWorkspace 0 >>= flip whenJust (windows . W.view) -- Focus the main screen again.

-- Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Below is a variation of the above except no borders are applied
-- if fewer than two windows. So a single window has no gaps.
mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

-- Defining a bunch of layouts, many that I don't use.
-- limitWindows n sets maximum number of windows displayed for layout.
-- mySpacing n sets the gap size around the windows.

tall     = renamed [Replace "tall"]
           $ smartBorders
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 16
           $ ResizableTall 1 (3/100) (1/2) []

magnify  = renamed [Replace "magnify"]
           $ smartBorders
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ magnifier
           $ limitWindows 12
           $ mySpacing 16
           $ ResizableTall 1 (3/100) (1/2) []

floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat

grid     = renamed [Replace "grid"]
           $ smartBorders
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 16
           $ mkToggle (single MIRROR)
           $ Grid (16/10)
           
spirals  = renamed [Replace "spirals"]
           $ smartBorders
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ mySpacing' 16
           $ spiral (6/7)

tabs     = renamed [Replace "tabs"]
           -- I cannot add spacing to this layout because it will
           -- add spacing between window and tabs which looks bad.
           $ tabbed shrinkText myTabTheme

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = "#46d9ff"
                 , inactiveColor       = "#313846"
                 , activeBorderColor   = "#46d9ff"
                 , inactiveBorderColor = "#282c34"
                 , activeTextColor     = "#282c34"
                 , inactiveTextColor   = "#d0d0d0"
                 }

-- Theme for showWName which prints current workspace when you change workspaces.
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font    = "xft:JetBrains Mono:bold:size=60"
    , swn_fade    = 1.0
    , swn_bgcolor = "#4c566a"
    , swn_color   = "#e5e9f0"
    }

-- The layout hook
myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout = withBorder myBorderWidth tall
                                 ||| magnify
                                 ||| noBorders tabs
                                 ||| grid
                                 ||| spirals

myWorkspaces :: [String]
myWorkspaces = ["MAIN", "SYS", "DEV", "WEB", "GFX", "6", "7", "DOCS", "TV"]

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
     -- 'doFloat' forces a window to float.  Useful for dialog boxes and such.
     -- using 'doShift ( myWorkspaces !! 7)' sends program to workspace 8!
     -- I'm doing it this way because otherwise I would have to write out the full
     -- name of my workspaces and the names would be very long if using clickable workspaces.
     [ isFullscreen             --> doFullFloat
     , className =? "confirm"         --> doCenterFloat
     , className =? "file_progress"   --> doCenterFloat
     , className =? "dialog"          --> doCenterFloat
     , className =? "download"        --> doCenterFloat
     , className =? "error"           --> doCenterFloat
     , className =? "Gimp"            --> doCenterFloat
     , className =? "notification"    --> doCenterFloat
     , className =? "pinentry-gtk-2"  --> doCenterFloat
     , className =? "splash"          --> doCenterFloat
     , className =? "toolbar"         --> doCenterFloat
     , className =? "Pavucontrol"     --> doCenterFloat
     , className =? "Matplotlib"      --> doCenterFloat
     , (className =? "zoom" <&&> resource =? "zoom")      --> doCenterFloat -- Float Zoom
     , (className =? "firefox" <&&> resource =? "Dialog") --> doCenterFloat -- Float Firefox Dialog

     , title =? "opencv" --> unfloat

     , className =? "Code"            --> doShift ( myWorkspaces !! 2 )
     , className =? "Firefox"         --> doShift ( myWorkspaces !! 3 )
     , className =? "Gimp"            --> doShift ( myWorkspaces !! 4 )
     
     , isFullscreen -->  doFullFloat
     ] where unfloat = ask >>= doF . W.sink

-- Key Bindings
myKeys = 
    -- Launch terminal
    [ ( (myModMask, xK_Return), spawn myTerminal )
    
    -- Launch rofi
    , ( (myModMask, xK_r), spawn "rofi -show drun" )

    -- Launch browser
    , ( (myModMask, xK_b), spawn "firefox" )

    -- Lauch vs-code
    , ( (myModMask, xK_c), spawn "code" )

    -- Launch file manager
    , ( (myModMask, xK_f), spawn myFileManager )

    -- Pulse audio volume control
    , ((myModMask, xK_v), spawn "pavucontrol")

    -- Volume control
    , ( (0, 0x1008FF11), spawn "amixer -q sset Master 2%-" )
    , ( (0, 0x1008FF13), spawn "amixer -q sset Master 2%+" )
    , ( (0, 0x1008FF12), spawn "amixer set Master toggle" )

    -- Layouts
    , ( (myModMask, xK_Tab),   sendMessage NextLayout )           -- Switch to next layout
    , ( (myModMask, xK_space), sendMessage (MT.Toggle NBFULL ) >> sendMessage ToggleStruts) -- Toggles noborder/full


    -- Close focused window
    , ( (myModMask .|. shiftMask, xK_q), kill )

    -- Quit xmonad
    , ( (myModMask .|. shiftMask, xK_r), io exitSuccess )
    ]
    ++
    [((m .|. myModMask, k), windows $ f i) -- To replace greedyView with regular view
         | (i, k) <- zip myWorkspaces [xK_1 .. xK_9]
         , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]
     
main :: IO ()
main = do
    xmproc <- spawnPipe "xmobar -x 0 $HOME/.config/xmobar/xmobarrc"
    xmprocdocs <- spawnPipe "xmobar -x 2 $HOME/.config/xmobar/xmobarrc-docs"
    -- xmproctv <- spawnPipe "xmobar -x 1 $HOME/.config/xmobar/xmobarrc-tv"
    
    -- the xmonad, ya know...what the WM is named after!
    xmonad $ ewmh def
        { manageHook         = myManageHook <+> manageDocks
        , handleEventHook    = docksEventHook
        , modMask            = myModMask
        , terminal           = myTerminal
        , startupHook        = myStartupHook
        , layoutHook         = showWName' myShowWNameTheme $ myLayoutHook
        , workspaces         = myWorkspaces
        , borderWidth        = myBorderWidth
        , normalBorderColor  = myNormalBorderColor
        , focusedBorderColor = myFocusedBorderColor

        , logHook = workspaceHistoryHook <+> dynamicLogWithPP xmobarPP
            { ppOutput = \x -> hPutStrLn xmproc x >> hPutStrLn xmprocdocs x -- >> hPutStrLn xmprocportrait x
            , ppCurrent          = xmobarColor "#d19a66" "" . wrap "|" "|"  -- current workspace cdd7e5
            , ppVisible          = xmobarColor "#ffffff" ""                 -- visible but not current workspace
            , ppVisibleNoWindows = Just(xmobarColor "#373c47" "")           -- visible but not current workspace (no windows)
            , ppHidden           = xmobarColor "#abb2bf" ""                 -- hidden workspaces
            , ppHiddenNoWindows  = xmobarColor "#282c34" ""                 -- hidden workspaces (no windows)
            , ppTitle            = xmobarColor "#d5d8df" "" . shorten 55    -- title of active window
            , ppSep              = "<fc=#4c566a> | </fc>"                   -- separators
            , ppUrgent           = xmobarColor "#e06c75" "" . wrap "!" "!"  -- urgent workspace
            , ppOrder            = \(ws:l:t:ex) -> [l,ws]++ex++[t]          -- ws : ex -- extra [ws,l]++ex++[t]
            }
        } `additionalKeys` myKeys
